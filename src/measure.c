#include "measure.h"


clock_t*
new_time(clock_t time)
{
  clock_t* n_time = (clock_t*)malloc(sizeof(clock_t));
  *n_time = time;
  return n_time;
}

clock_t
measure_func_time(void (*func)(void))
{
  clock_t start = clock();
  func();
  clock_t end = clock();

  return end - start;
}

int
measure_func_times(void (*func)(void), size_t n, clock_t* results)
{
  if (func == NULL || results == NULL) {
    return EXIT_FAILURE;
  }

  for (size_t i = 0; i < n; ++i) {
    results[i] = measure_func_time(func);
  }

  return EXIT_SUCCESS;
}


clock_t
times_average(clock_t* times, size_t n)
{
  double avg_d = 0;
  for (int i = 0; i < n; ++i) {
    avg_d += (double)times[i] / n;
  }

  return (clock_t)avg_d;
}


double
time_in_secs(clock_t time)
{
  return ((double)time) / CLOCKS_PER_SEC;
}
